import java.util.Random;
public class RandomArray{

	private int array[];

	public RandomArray(int n){
		this.array = new int[n];
		Random r = new Random();
		for(int i=0;i<n;i++){
			this.array[i] = r.nextInt();
		}
	}

	public int[] getArray(){
		return this.array;
	}
}
